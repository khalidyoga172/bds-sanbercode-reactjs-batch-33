//soal 1
var nilaiJohn = 80;
var nilaiDoe = 50;
let indeksJohn;
let indeksDoe;

if (nilaiJohn >= 80)
  indeksJohn = "A";
else if (nilaiJohn < 80 & nilaiJohn >= 70)
  indeksJohn = "B";
else if (nilaiJohn < 70 & nilaiJohn >= 60)
  indeksJohn = "C";
else if (nilaiJohn < 60 & nilaiJohn >= 70)
  indeksJohn = "D";
else if (nilaiJohn < 50)
  indeksJohn = "E";
else indeksJohn = '';

if (nilaiDoe >= 80)
  indeksDoe = "A";
else if (nilaiDoe < 80 & nilaiDoe >= 70)
  indeksDoe = "B";
else if (nilaiDoe < 70 & nilaiDoe >= 60)
  indeksDoe = "C";
else if (nilaiDoe < 60 & nilaiDoe >= 50)
  indeksDoe = "D";
else if (nilaiDoe < 50)
  indeksDoe = "E";


console.log("indeks nilai john adalah " + indeksJohn + " dan indeks nilai doe adalah " + indeksDoe);

//soal 2
var tanggal = 18;
var bulan = 8;
var tahun = 1998;
let bulanTxt;
switch (bulan) {
  case 1: { bulanTxt = "Januari"; break; }
  case 2: { bulanTxt = "Frebuari"; break; }
  case 3: { bulanTxt = "Maret"; break; }
  case 4: { bulanTxt = "April"; break; }
  case 5: { bulanTxt = "Mei"; break; }
  case 6: { bulanTxt = "Juni"; break; }
  case 7: { bulanTxt = "Juli"; break; }
  case 8: { bulanTxt = "Agustus"; break; }
  case 9: { bulanTxt = "September"; break; }
  case 10: { bulanTxt = "Oktober"; break; }
  case 11: { bulanTxt = "November"; break; }
  case 12: { bulanTxt = "Desember"; break; }
  default: { console.log('Tidak terjadi apa-apa'); }
}
console.log(tanggal + " " + bulanTxt + " " + tahun);


//soal 3
let coding = "I love coding";
let fe = "I will become a frontend developer";
let i = 0;
let x = 0;
while (i <= 20) {
  if (i < 10) {
    console.log(((i + 1) * 2) + " - " + coding);
  }
  else if (i >= 10)
    console.log((40 - i - x) + " - " + fe);
  x++;
  i++;
}

//soal 4
for (i = 1; i <= 20; i++) {
  if (i % 3 === 0 & i % 2 !== 0)
    console.log(i + " - I love coding");
  else if (i % 2 === 0)
    console.log(i + " - berkualitas");
  else if (i % 2 !== 0)
    console.log(i + " - santai");
}

//soal 5
let sym="";
for(i = 1; i<= 7; i++){
  sym = sym + "#";
  console.log(sym);
}

