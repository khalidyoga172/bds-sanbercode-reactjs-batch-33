//soal 1
function luasPersegiPanjang(x, y) {
  return x * y;
}
function kelilingPersegiPanjang(x, y) {
  return 2 * (x + y);
}
function volumeBalok(x, y, z) {
  return x * y * z;
}

let panjang = 12
let lebar = 4
let tinggi = 8

let HasilluasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
let HasilkelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
let HasilvolumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(HasilluasPersegiPanjang)
console.log(HasilkelilingPersegiPanjang)
console.log(HasilvolumeBalok)



//soal 2
const introduce = (...rest) => {
  let [name, age, gender, job] = rest
  if(gender==="Laki-Laki")
    return `pak ${name} adalah seorang ${job} yang berusia ${age}`
  else if(gender==="Perempuan")
    return `bu ${name} adalah seorang ${job} yang berusia ${age}  `
}

const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan)



//soal 3
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992]
let objDaftarPeserta = {
  name: "John Doe",
  gender: "laki-laki",
  hobby: "baca buku",
  born: 1992
}
console.log(objDaftarPeserta)

//soal 4
let buah = [
  {
    nama: "Nanas",
    warna: "Kuning",
    biji: false,
    harga: 9000
  },
  {
    nama: "Jeruk",
    warna: "Oranye",
    biji: true,
    harga: 8000
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    biji: true,
    harga: 10000
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    biji: false,
    harga: 5000
  },
]

var buahFilter = buah.filter(function(item){
  return item.biji == false;
})
console.log(buahFilter);

//soal 5
let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}

const colors = phone.colors;
const phoneName = phone.name;
const phoneBrand = phone.brand;
const year = phone.year;

const colorBronze = phone.colors[0];
const colorWhite = phone.colors[1];
const colorBlack = phone.colors[2];

console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)


//soal 6
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

buku.warnaSampul.push(...warna);
let newBook = {...buku, ...dataBukuTambahan};

console.log(newBook);



//soal 7
// function tambahDataFilm (nama, durasi, genre, tahun){
//   datafilm.push(nama,durasi,genre,tahun);
// }

const tambahDataFilm = (...rest) =>{ 
  
  const [nama, durasi, genre, tahun] = rest;
  var temp={};
  temp.nama = nama;  
  temp.durasi = durasi;
  temp.genre = genre;
  temp.tahun = tahun;

  dataFilm.push(temp);
} ;

let dataFilm = [];

tambahDataFilm("LOTR", "2 jam", "action", "1999");
tambahDataFilm("avenger", "2 jam", "action", "2019");
tambahDataFilm("spiderman", "2 jam", "action", "2004");
tambahDataFilm("juon", "2 jam", "horror", "2004");

console.log(dataFilm);