import QuizRouter from './router';

function App() {
  return (
    <>
    <QuizRouter/>
    </>
  );
}

export default App;
