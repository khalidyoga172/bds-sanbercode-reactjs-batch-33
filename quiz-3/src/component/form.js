import React, { useContext, useEffect } from "react"
import { MovieContext } from "../context"
import axios from "axios"
import { useParams } from "react-router-dom"

const Form = () => {
  const { functions, movie, setMovie, inputMovie, setInputMovie, setIndex } = useContext(MovieContext)
  const { fetchData, platformAndroid, platformIos, functionHandleEdit, functionHandleDelete, functionHandleChange, functionHandleSubmit } = functions
  let { id } = useParams();

  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    functionHandleChange(name, value)
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    functionHandleSubmit()
  }

  useEffect(() => {
    axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
      .then(res => {
        let newData = res.data
        setInputMovie(newData)
        setIndex(newData.id)
      })
  }, [])

  return (
    <>
      <div className="bg-stone-200  p-10">
        <div className="w-11/12 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <form onSubmit={handleSubmit} method="POST" id="form1">
            <div className=" relative mt-2">
              <label htmlFor="name" className="text-gray-700 block">
                Nama
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.name} required type="text" id="name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="name" placeholder="Nama" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="category" className="text-gray-700 block">
                Kategori
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.category} required type="text" id="category" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="category" placeholder="Kategori" />
            </div>
            <div className=" relative mt-2">
              <label className="text-gray-700" htmlFor="description">
                Deskripsi
                <textarea onChange={handleChange} value={inputMovie.description} className="flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 rounded-lg text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" id="description" placeholder="Deskripsi" name="description" rows={5} cols={40} defaultValue={""} />
              </label>
            </div>
            <div className=" relative mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                Tahun Rilis
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.release_year} required type="number" id="release_year" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="release_year" min="2007" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="size" className="text-gray-700 block">
                Ukuran
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.size} required type="number" id="size" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="size" placeholder="dalam satuan Mb" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="price" className="text-gray-700 block">
                Harga
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.price} required type="number" id="price" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="price" placeholder="0" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="rating" className="text-gray-700 block">
                Rating
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.rating} required type="number" id="rating" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="rating" max="5" min="0" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="image_url" className="text-gray-700 block">
                Image url
                <span className="text-red-500 required-dot">
                  *
                </span>
              </label>
              <input onChange={handleChange} value={inputMovie.image_url} required type="text" id="image_url" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-1/3 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="image_url" />
            </div>
            <div className=" relative mt-2">
              <label className="text-gray-700 block">
                Platform
              </label>
              <input onChange={handleChange} type="checkbox" id="is_android_app" name="is_android_app" value={inputMovie.is_android_app ? 0 : 1} check={inputMovie.is_android_app ? true : false}/>
              <label for="is_android_app"> Android</label><br />
              <input onChange={handleChange} type="checkbox" id="is_ios_app" name="is_ios_app" value={inputMovie.is_ios_app ? 0 : 1} check={inputMovie.is_ios_app ? true : false} />
              <label for="is_ios_app"> IOS</label><br />
            </div>
            <div className="submit">
              <button form="form1" value="Submit" className="mt-10 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div >
    </>
  )
}
export default Form