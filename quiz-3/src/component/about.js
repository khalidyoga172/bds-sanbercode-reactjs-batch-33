import React from "react"

const About = () => {
  return (
    <>
      <div className="bg-stone-200 h-screen p-10">
        <div className="w-11/12 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <div className="border-2 p-4">
            <div className="my-0 mx-auto text-center text-2xl">
              <b>Data Peserta Sanbercode Bootcamp Reactjs</b><br/><br/>
            </div>
            <ul>
              <li><b>Nama:</b> Khalid Yoga Hidayatullah</li>
              <li><b>Email:</b> khalidyoga172@gmail.com</li>
              <li><b>Sistem Operasi yang digunakan:</b> windows 7 & 10</li>
              <li><b>Akun Gitlab:</b> https://gitlab.com/khalidyoga172</li>
              <li><b>Akun Telegram:</b> https://t.me/Khalidyoga</li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}
export default About