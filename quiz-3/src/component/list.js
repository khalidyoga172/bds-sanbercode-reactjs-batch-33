import React, { useContext, useEffect, useState } from "react"
import { MovieContext } from "../context"
import {
  Link
}from "react-router-dom";
const List = () => {
  const { functions, movie } = useContext(MovieContext)
  const { fetchData, platformAndroid, platformIos, functionHandleEdit,functionHandleDelete } = functions

  useEffect(() => {
    fetchData()
  }, [])

  const handleEdit = (event) => {
    let id = event.target.value
    functionHandleEdit(id)
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    functionHandleDelete(index)

  }
  return (
    <>
      <div className="w-5/6 my-0 mx-auto ">
        <table className="table p-4 bg-white mt-10 w-auto">
          <thead>
            <tr>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                No
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Name
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Category
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Description
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Release Year
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Size
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Price
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Rating
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Platform
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
                Action
              </th>
              <th className="border-b-2 p-4 dark:border-dark-5   font-normal text-gray-900">
              </th>
            </tr>
          </thead>
          <tbody>
            {movie !== null && (
              <>
                {movie.map((e, index) => {
                  return (
                    <tr key={index} className="text-gray-700">
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.id}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.name}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.category}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.description}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.release_year}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.size}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.price}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {e.rating}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        {platformAndroid(e)}
                        {platformIos(e) && platformAndroid(e) ? <br /> : <></>}
                        {platformIos(e)}
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        <p className="text-gray-900 whitespace-no-wrap">
                          <button onClick={handleEdit} value={e.id} type="button" className="py-2 px-4 flex justify-center items-center  bg-yellow-300 hover:bg-yellow-500 focus:ring-yellow-500 focus:ring-offset-yellow-200 text-black w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                            Edit
                          </button>
                        </p>
                      </td>
                      <td className="border-b-2 p-4 dark:border-dark-5">
                        <p className="text-gray-900 whitespace-no-wrap">
                          <button onClick={handleDelete} value={e.id} type="button" className="py-2 px-4 flex justify-center items-center  bg-red-700 hover:bg-red-800 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                            Delete
                          </button>
                        </p>
                      </td>
                    </tr>
                  )
                })}
              </>
            )}
          </tbody>
        </table>
        <button type="button" className="mt-6 w-auto py-2 px-4 flex justify-center items-center  bg-green-600 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
          <svg width={20} height={20} fill="currentColor" className="mr-2" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
            <path d="M1344 1472q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm256 0q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm128-224v320q0 40-28 68t-68 28h-1472q-40 0-68-28t-28-68v-320q0-40 28-68t68-28h427q21 56 70.5 92t110.5 36h256q61 0 110.5-36t70.5-92h427q40 0 68 28t28 68zm-325-648q-17 40-59 40h-256v448q0 26-19 45t-45 19h-256q-26 0-45-19t-19-45v-448h-256q-42 0-59-40-17-39 14-69l448-448q18-19 45-19t45 19l448 448q31 30 14 69z">
            </path>
          </svg>
          <Link to="/mobile-form">Input Data</Link>          
        </button>

      </div>
    </>
  )
}
export default List