import React, { useContext, useEffect, useState } from "react"
import { MovieContext } from "../context"

const SearchPage = () => {
  const { functions, movie, search, hasilSearch, setHasilSearch } = useContext(MovieContext)
  const { fetchData, platformAndroid, platformIos } = functions

  useEffect(() => {
    let data = movie.filter(e => e.name === search)
    // let data = [{a: "a"},{a:"b"}]
    // setHasilSearch(data)
    setHasilSearch(data.map(e => {
      // console.log(e)
      return {
        id: e.id,
        name: e.name,
        category: e.category,
        description: e.description,
        release_year: e.release_year,
        size: e.size,
        price: e.price,
        rating: e.rating,
        is_android_app: e.is_android_app,
        is_ios_app: e.is_ios_app,
        image_url: e.image_url
      }
    }))
    console.log(movie)
    console.log(hasilSearch)
  }, [])

  const priceFunction = (e) => {
    if (e === 0)
      return "free"
    return "Rp. " + e + ",-"
  }
  const sizing = (e) => {
    if (e / 1000 > 0)
      return e / 1000 + " Gb"
    else if (e / 1000 === 0)
      return e + " Mb"
  }

  return (
    <>
      <div className="bg-stone-200 pt-10">
        <main className="p-8 w-11/12 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          {movie !== null && (
            <>
              {movie.map((e, index) => {
                return (
                  <>
                    <div className="mb-20">
                      {e.name === search && (
                        <>
                          <div className="font-bold text-3xl" >{e.name}</div>
                          <small className="text-slate-500">released year: {e.release_year}</small>
                          <div className="w-96">
                            <img src={e.image_url} />
                          </div>
                          <b>
                            <table key={index} className="font-mono text-slate-800">
                              <tr>
                                <td className="w-24"> price </td>
                                <td> : {priceFunction(e.price)}</td>
                              </tr>
                              <tr>
                                <td> rating </td>
                                <td> : {e.rating}</td>
                              </tr>
                              <tr>
                                <td> size </td>
                                <td> : {sizing(e.size)}</td>
                              </tr>
                              <tr>
                                <td> platform </td>
                                <td> : {platformAndroid(e)}{platformIos(e) && platformAndroid(e) ? " & " : <></>}{platformIos(e)}</td>
                              </tr>
                              <tr>
                                <td> description  </td>
                                <td> : </td>
                              </tr>
                            </table>

                          </b>
                          <small> {e.description}</small>
                        </>
                      )}
                    </div>
                  </>
                )
              })}
            </>
          )}
        </main>
      </div>

    </>
  )
}
export default SearchPage