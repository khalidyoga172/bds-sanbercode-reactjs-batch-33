import React, { useContext, useEffect } from "react"
import { MovieContext } from "../context"

const Home = () => {
  const { functions, movie } = useContext(MovieContext)
  const { fetchData, platformAndroid, platformIos } = functions

  useEffect(() => {
    fetchData()
  }, [])

  const priceFunction = (e) => {
    if (e === 0)
      return "free"
    return "Rp. " + e + ",-"
  }
  const sizing = (e) => {
    if (e / 1000 > 0)
      return e/1000 + " Gb"
    else if (e / 1000 === 0)
      return e + " Mb"
  }

  return (
    <>
      <div className="bg-stone-200 pt-10">
        <main className="p-8 w-11/12 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          {movie !== null && (
            <>
              {movie.map((e, index) => {
                return (
                  <>
                    <div  key ={e.id} className="mb-20">
                      <div className="font-bold text-3xl" key={index}>{e.name}</div>
                      <small className="text-slate-500">released year: {e.release_year}</small>
                      <div className="w-96">
                        <img src={e.image_url} />
                      </div>
                      <b>
                        <table className="font-mono text-slate-800">
                          <tr>
                            <td className="w-24"> price </td>
                            <td> : {priceFunction(e.price)}</td>
                          </tr>
                          <tr>
                            <td> rating </td>
                            <td> : {e.rating}</td>
                          </tr>
                          <tr>
                            <td> size </td>
                            <td> : {sizing(e.size)}</td>
                          </tr>
                          <tr>
                            <td> platform </td>
                            <td> : {platformAndroid(e)}{platformIos(e) && platformAndroid(e) ? " & " : <></>}{platformIos(e)}</td>
                          </tr>
                          <tr>
                            <td> description  </td>
                            <td> : </td>
                          </tr>
                        </table>

                      </b>
                      <small> {e.description}</small>
                    </div>
                  </>
                )
              })}
            </>
          )}
        </main>
      </div>

    </>
  )
}
export default Home