import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from "react-router-dom";
import { MovieProvider } from "./context";
import Nav from "./nav"
import Home from "./component/home"
import List from "./component/list"
import About from "./component/about";
import Form from "./component/form";
import SearchPage from "./component/search"

const QuizRouter = ()=>{
  return(
    <Router>
      <MovieProvider>
      <Nav/>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/mobile-list" exact component={List} />
        <Route path="/about" exact component={About} />
        <Route path="/mobile-form" exact component={Form} />
        <Route path="/mobile-form/edit/:id" exact component={Form} />
        <Route path="/search/:valueOfSearch" exact component={SearchPage} />
      </Switch>
      </MovieProvider>
    </Router>
  )
}
export default QuizRouter