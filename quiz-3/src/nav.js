import React, { useContext } from "react"
import {
  Link
} from "react-router-dom";
import { MovieContext } from "./context";
import logo from "./img/logo.png"

const Nav = () => {
  const { search, setSearch, functions } = useContext(MovieContext)
  const { searchFunction } = functions

  const handleChange = (e) => {
    // console.log("search =" + search)
    let data = e.target.value
    // console.log("target ="+ data)

    setSearch( data )
    // console.log("search after=" + search)

  }
  const cari = (event) =>{
    event.preventDefault()
    searchFunction()
  }

  return (
    <div>
      <nav className="bg-white dark:bg-gray-800  shadow py-4 ">
        <div className="max-w-7xl mx-auto px-8">
          <div className="flex items-center justify-between">
            <div className=" flex items-center">
              <a className="flex-shrink-0" href="/">
                <img className="h-10 " src={logo} alt="Workflow" />
              </a>
              <div className="hidden md:block">
                <div className="ml-10 flex items-baseline space-x-4">
                  <Link to="/" className="text-gray-800 dark:text-white hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                    Home
                  </Link>
                  <Link to="/mobile-list" className="text-gray-800 dark:text-white hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                    Movie List
                  </Link>
                  <Link to="/about" className="text-gray-800 dark:text-white hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                    About
                  </Link>
                </div>
              </div>
            </div>
            <div className="block">
              <div className="md:block -mr-2 flex">
                <form onSubmit={cari} method="POST" className="flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
                  <div className=" relative ">
                    <input onChange={handleChange} value={search} type="text"  className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Cari Nama . . ." />
                  </div>
                  <button className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-purple-600 rounded-lg shadow-md hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-500 focus:ring-offset-2 focus:ring-offset-purple-200" type="submit">
                      Search
                  </button>
                </form>
              </div>
              <div className="ml-4 flex items-center md:ml-6">
              </div>
            </div>
            <div className="-mr-2 flex md:hidden">
              <button className="text-gray-800 dark:text-white hover:text-gray-300 inline-flex items-center justify-center p-2 rounded-md focus:outline-none">
                <svg width={20} height={20} fill="currentColor" className="h-8 w-8" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1664 1344v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45z">
                  </path>
                </svg>
              </button>
            </div>
          </div>
        </div>

      </nav>
    </div>

  )
}
export default Nav