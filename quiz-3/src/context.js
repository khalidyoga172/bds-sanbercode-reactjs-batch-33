import React, { useState, createContext, useContext } from "react";
import axios from "axios"
import history, { useHistory } from "react-router-dom";

export const MovieContext = createContext();
export const MovieProvider = props => {
  const [movie, setMovie] = useState([])
  const [hasilSearch,setHasilSearch] = useState([])
  const [search, setSearch] = useState("")
  const [inputMovie, setInputMovie] = useState({
    name: "",
    category: "",
    description: "",
    release_year: "",
    size: "",
    price: "",
    rating: "",
    image_url: "",
    is_android_app:0, 
    is_ios_app:0
  })
  const [index, setIndex] = useState(-1)
  let history = useHistory()

  const fetchData = async () => {
    let result = await axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps`)
    // console.log(result)
    setMovie(result.data.map(e => {
      return {
        id: e.id,
        name: e.name,
        category: e.category,
        description: e.description,
        release_year: e.release_year,
        size: e.size,
        price: e.price,
        rating: e.rating,
        is_android_app: e.is_android_app,
        is_ios_app: e.is_ios_app,
        image_url: e.image_url
      }
    }))
    setHasilSearch(result.data.map(e => {
      return {
        id: e.id,
        name: e.name,
        category: e.category,
        description: e.description,
        release_year: e.release_year,
        size: e.size,
        price: e.price,
        rating: e.rating,
        is_android_app: e.is_android_app,
        is_ios_app: e.is_ios_app,
        image_url: e.image_url
      }
    }))
    // console.log(result.data)
  }
  const platformAndroid = (e) => {
    if (e.is_android_app)
      return "Android"
  }
  const platformIos = (e) => {
    if (e.is_ios_app)
      return "IOS"
  }

  const functionHandleEdit = (id) => {
    console.log(inputMovie)
    history.push(`mobile-form/edit/${id}`)
  }

  const functionHandleDelete = (id) => {
    axios.delete(`https://backendexample.sanbercloud.com/api/mobile-apps/{id}`)
      .then(() => {
        let newData = movie.filter(el => { return el.id !== id })
        setMovie(newData)
      })
  }

  const functionHandleChange = (name, value) => {
    console.log(value)
    setInputMovie({ ...inputMovie, [name]: value })
  }

  const functionHandleSubmit = () => {
    //tambah
    let { name, category, description, release_year, size, price, rating, image_url,is_android_app, is_ios_app } = inputMovie

    if (index < 0) {

      axios.post(`https://backendexample.sanbercloud.com/api/mobile-apps`, {
        name,
        category,
        description,
        release_year,
        size,
        price,
        rating,
        image_url,
        is_android_app, 
        is_ios_app
      }
      ).then(res => {
        let data = res.data
        setMovie([...movie, {
          name: inputMovie.name,
          category: inputMovie.category,
          description: inputMovie.description,
          release_year: inputMovie.release_year,
          size: inputMovie.size,
          price: inputMovie.price,
          rating: inputMovie.rating,
          image_url: inputMovie.image_url,
          is_android_app: inputMovie.is_android_app, 
          is_ios_app: inputMovie.is_ios_app
        }])
        history.push('/')
      })
    }
    //edit
    else if (index >= 0) {
      console.log(inputMovie)
      axios.put(`https://backendexample.sanbercloud.com/api/mobile-apps/${index}`, {
        name, 
        category, 
        description, 
        release_year, 
        size, 
        price, 
        rating, 
        image_url,
        is_android_app, 
        is_ios_app
      })
        .then(() => {
          let updateData = movie.find(e => e.id === index)
          updateData.name = inputMovie.name
          updateData.category = inputMovie.category
          updateData.description = inputMovie.description
          updateData.release_year = inputMovie.release_year
          updateData.size = inputMovie.size
          updateData.price = inputMovie.price
          updateData.rating = inputMovie.rating
          updateData.image_url = inputMovie.image_url
          is_android_app =inputMovie.is_android_app
          is_ios_app=inputMovie.is_ios_app
          // console.log(updateData)
          setMovie([...movie])
          history.push('/')
        })
    }
    //kosong
    setIndex(-1)
    setInputMovie({
      name: "",
      category: "",
      description: "",
      release_year: "",
      size: "",
      price: "",
      rating: "",
      image_url: "",
      is_android_app:0, 
      is_ios_app:0
    })
  }

  const searchFunction = ()=>{
    {search && history.push(`/search/${search}`)}
    
    // console.log(search)
  }

  const functions = {
    fetchData,
    platformAndroid,
    platformIos,
    functionHandleEdit,
    functionHandleDelete,
    functionHandleChange,
    functionHandleSubmit,
    searchFunction
  }
  return (
    <MovieContext.Provider value={{
      movie, setMovie,
      index, setIndex,
      inputMovie, setInputMovie,
      search,setSearch,
      hasilSearch,setHasilSearch,
      functions
    }}>
      {props.children}
    </MovieContext.Provider>
  )
}