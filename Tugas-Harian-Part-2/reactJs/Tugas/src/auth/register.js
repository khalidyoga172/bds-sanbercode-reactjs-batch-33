import React, { useContext } from "react"
import { NilaiContext } from "../tugas-13/nilaiMahasiswaContext"

const Register = () => {

  const { userData, functions } = useContext(NilaiContext)
  const { handleChangeAuth, handleRegister } = functions

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value
    handleChangeAuth(name, value)
  }

  return (
    <>
      <div className="bg-stone-200  p-10">
        <div className="w-1/3 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <form onSubmit={handleRegister} method="POST" id="form1">
            <div className=" relative mt-2">
              <label htmlFor="name" className="text-gray-700 block">
                Nama

              </label>
              <input onChange={handleChange} value={userData.nama} required type="text" id="nama" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="nama" placeholder="Nama" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="email" className="text-gray-700 block">
                Email
              </label>
              <input onChange={handleChange} value={userData.email} required type="text" id="email" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="email" placeholder="email" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                Password
              </label>
              <input onChange={handleChange} value={userData.password} required maxlength="8" type="password" id="password" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="password" />
            </div>
            <div className="submit">
              <button form="form1" value="Submit" className="mt-10 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div >
    </>
  )
}
export default Register