import react from 'react';
import logo from './img/logo.png';
import './tugas10.css';

const Tugas10 = () => {
  return (
    <div className="App">
      <div className="content">
        <img src={logo} />
        <h1>THINGS TO DO</h1>
        <div className="title">
          During bootcamp in sanbercode;
        </div>
        <div className="checkBox">
          <form>
            <div className="checkBox-list">
              <input type="checkbox" id="git" name="git" />
              <label for="git">Belajar GIT & CLI</label><br />
            </div>
            <div className="checkBox-list">
              <input type="checkbox" id="html" name="html" />
              <label for="html">Belajar HTML & CSS</label><br />
            </div>
            <div className="checkBox-list">
              <input type="checkbox" id="javascript" name="javascript" />
              <label for="javascript">Belajar Javascript</label><br />
            </div>
            <div className="checkBox-list">
              <input type="checkbox" id="reactDasar" name="reactDasar" />
              <label for="reactDasar">Belajar ReactJS Dasar</label><br />
            </div>
            <div className="checkBox-list">
              <input type="checkbox" id="reactAdvance" name="reactAdvance" />
              <label for="reactAdvance">Belajar ReactJS Advance</label><br />
            </div>
            <div className='submit'>
              <input type="submit" value="SEND" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Tugas10;
