import React, { useContext, useEffect, useState } from "react"
import {
  Link
} from "react-router-dom";
import { NilaiContext } from "../tugas-13/nilaiMahasiswaContext";
import logo from '../tugas-10/img/logo.png';
import Cookies from "js-cookie";
import history, { useHistory } from "react-router-dom"
import NilaiMahasiswa from "../tugas-13/nilaiMahasiswa";


const Nav = () => {
  // const { darkMode, setDarkMode } = useContext(NilaiContext);
  let dark = "bg-white dark:bg-gray-800  shadow py-4"
  let light = "bg-white bg-white-800  shadow py-4"
  const { functions } = useContext(NilaiContext)
  const { handleLogout } = functions
  const [darkMode, setDarkMode] = useState(light);
  let history = useHistory()


  const changeTheme = () => {
    darkMode == dark
      ? setDarkMode(light)
      : setDarkMode(dark);
    console.log(darkMode)
  }

  return (
    <>
      <div>
        <nav className={darkMode}>
          <div className="max-w-full mx-auto px-8 ">
            <div className="flex items-center justify-between h-14">
              <div className=" flex items-center ">
                <a className="flex-shrink-0 " href="/">
                  <img className="mb-10 w-60" src={logo} alt="Workflow" />
                </a>
                <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
                    <ul>
                    <Link to="/" className="text-gray-300  hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Home
                    </Link>
                    <Link to="/tugas10" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium" >
                      Tugas 10
                    </Link>
                    <Link to="/tugas11" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Tugas 11
                    </Link>
                    <Link to="/tugas12" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Tugas 12
                    </Link>
                    <Link to="/tugas13" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Tugas 13
                    </Link>
                    <Link to="/tugas14" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Tugas 14
                    </Link>
                    <Link to="/tugas15" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Tugas 15
                    </Link>
                    <Link to="/register" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                      Register
                    </Link>
                    {!Cookies.get('token') &&
                      <Link to="/login" className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                        Login
                      </Link>
                    }
                    {Cookies.get('token') &&
                      <button onClick={handleLogout} className="text-gray-300 hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md text-md font-medium">
                        logout
                      </button>
                    }
                    </ul>
                  </div>
                </div>
              </div>
              <div className="block">
                <div className="md:block -mr-2 flex">
                  <button onClick={changeTheme} className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-blue-600 rounded-lg shadow-md hover:bg-blue-700 focus:outline-none ">
                    Change Theme
                  </button>
                </div>
                <div className="ml-4 flex items-center md:ml-6">
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </>
  )
}
export default Nav