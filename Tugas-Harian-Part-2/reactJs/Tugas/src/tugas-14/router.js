import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas10 from "../tugas-10/Tugas10";
import Tugas11 from "../tugas-11/Tugas11";
import Tugas12 from "../tugas-12/Tugas12";
import Tugas13 from "../tugas-13/nilaiMahasiswa";
import Tugas14Tabel from "./tugas14Tabel";
import Tugas15Tabel from "../tugas-15/tugas15Tabel";
import Login from "../auth/login.js";
import Register from "../auth/register.js";
import Form from "../tugas-15/tugas15Form"
import Nav from './nav'
import { NilaiProvider } from "../tugas-13/nilaiMahasiswaContext"
// import { createGlobalStyle } from 'styled-components';
// import '../tugas-11/tugas11.css';
// import './tugas14.css';

const RouterT14 = () => {
  return (
    <Router>
        <NilaiProvider>
          <Nav />
          <Switch>
            <Route path="/" exact component={Tugas10} />
            <Route path="/tugas10" exact component={Tugas10} />
            <Route path="/tugas11" exact component={Tugas11} />
            <Route path="/tugas12" exact component={Tugas12} />
            <Route path="/tugas13" exact component={Tugas13} />
            <Route path="/tugas14" exact component={Tugas14Tabel} />
            <Route path="/tugas14/create" exact component={Form} />
            <Route path="/tugas14/edit/:slug" exact component={Form} />
            <Route path="/tugas15" exact component={Tugas15Tabel} />
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
          </Switch>
        </NilaiProvider>

    </Router>
  );
}
export default RouterT14
