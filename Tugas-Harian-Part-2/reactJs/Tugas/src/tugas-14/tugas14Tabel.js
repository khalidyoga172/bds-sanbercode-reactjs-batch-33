import React, { useContext, useEffect } from "react"
import { NilaiContext } from "../tugas-13/nilaiMahasiswaContext"
import {
  Link
} from "react-router-dom";
// import './tugas14.css'
import { useHistory } from "react-router-dom";

const Tugas14Tabel = () => {

  const { nilaiMahasiswa, setNilaiMahasiswa, inputNilaiMahasiswa, setInputNilaiMahasiswa, currentIndex, setCurrentIndex, functions,darkMode,setDarkMode } = useContext(NilaiContext)
  const { fetchData, functionIndeksNilai, functionHandleEdit, functionHandleDelete, functionHandleChange, functionHandleSubmit } = functions
  let history = useHistory();

  useEffect(() => {
    fetchData()
  }, [])

  const inputIndeksNilai = (e) => {
    return functionIndeksNilai(e)
  }


  const handleEdit = (event) => {
    let id = event.target.value
    functionHandleEdit(id)
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    functionHandleDelete(index)

  }
  const changeTheme = () => {
    darkMode === true
    ? setDarkMode(false)
    :setDarkMode(true);
    console.log(darkMode)
  }

  return (
    <>
      <div className="buttonTheme">
        <button onClick={changeTheme}>Change Theme</button>
      </div>
      <h1>Daftar Nilai Mahasiswa</h1>
      <div className="button">
        <button><Link to="/tugas14/create">Buat Nilai Mahasiswa Baru</Link></button>
      </div>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Mata Kuliah</th>
            <th>Nilai</th>
            <th>Indeks Nilai</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {nilaiMahasiswa !== null && (
            <>
              {nilaiMahasiswa.map((e, index) => {
                return (
                  <tr key={index}>
                    <td>{e.id}</td>
                    <td>{e.name}</td>
                    <td>{e.course}</td>
                    <td>{e.score}</td>
                    <td>{inputIndeksNilai(e)}</td>
                    <td>
                      <button onClick={handleEdit} value={e.id}>Edit</button>
                      <button onClick={handleDelete} value={e.id}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </>
          )}

        </tbody>
      </table><br /><br />


    </>
  )
}

export default Tugas14Tabel