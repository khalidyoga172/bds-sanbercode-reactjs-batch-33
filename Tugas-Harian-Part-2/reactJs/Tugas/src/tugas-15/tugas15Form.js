import React, { useContext, useEffect } from "react"
import { NilaiContext } from "../tugas-13/nilaiMahasiswaContext"
import axios from "axios"
import { useParams } from "react-router-dom"

const Form = () => {

  const {inputNilaiMahasiswa, setInputNilaiMahasiswa, setCurrentIndex, functions } = useContext(NilaiContext) 
  const { functionHandleChange, functionHandleSubmit } = functions
  let { slug } = useParams();

  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    functionHandleChange(name, value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    functionHandleSubmit()
  }

  useEffect(() => {
    const fetchData = async () => {
      let result = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
      axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${slug}`)
        .then(res => {
          let newData = res.data
          setInputNilaiMahasiswa(newData)
          setCurrentIndex(newData.id)
        })
    }
    fetchData()
  }, [])

  return (
    <>
      <h1>Form Nilai Mahasiswa</h1>
      <div className="form">
        <form onSubmit={handleSubmit} method="POST">
          <div className="row">
            <label>Nama </label>
            <input type="text" value={inputNilaiMahasiswa.name} name="name" onChange={handleChange} required /><br />
          </div>
          <div className="row">
            <label>Mata Kuliah:</label>
            <input type="text" value={inputNilaiMahasiswa.course} name="course" onChange={handleChange} required /><br />
          </div>
          <div className="row">
            <label>Nilai</label>
            <input type="number" value={inputNilaiMahasiswa.score} name="score" onChange={handleChange} min="0" max="100" required /><br />
          </div>
          <div className="submit">
            <input type="submit" value="submit" />
          </div>
        </form>
      </div>
    </>
  )
}
export default Form