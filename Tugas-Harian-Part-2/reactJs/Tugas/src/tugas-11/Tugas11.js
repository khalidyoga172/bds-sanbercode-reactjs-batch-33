import React, { useState } from "react"

const Tugas11 = () => {

  const [daftarBuah, setDaftarBuah] = useState([
    { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
    { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
    { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
    { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
    { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 }
  ])
  const [inputBuah, setInputBuah] = useState({ nama: "", hargaTotal: "", beratTotal: "" })
  const [currentIndex, setCurrentIndex] = useState(-1)

  const handleEdit = (event) => {
    let index = parseInt(event.target.value)
    setInputBuah(daftarBuah[index])
    setCurrentIndex(event.target.value)
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    let deletedItem = daftarBuah[index]
    let newData = daftarBuah.filter((e) => { return e !== deletedItem })
    setDaftarBuah(newData)
  }

  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name

    setInputBuah({ ...inputBuah, [name]: value })
    console.log(inputBuah)
  }


  const handleSubmit = (event) => {
    event.preventDefault()
    let { nama, hargaTotal, beratTotal } = inputBuah
    let newData = daftarBuah

    console.log(beratTotal + "inputBuah" + currentIndex)

    //tambah
    if (beratTotal >= 2000 && currentIndex < 0 && (nama && hargaTotal && beratTotal !== null)) {
      newData = [...daftarBuah, { nama, hargaTotal, beratTotal }]
      setDaftarBuah(newData)
    }
    //edit
    else if (beratTotal >= 2000 && currentIndex >= 0 && (nama && hargaTotal && beratTotal !== null)) {
      daftarBuah[currentIndex].nama = nama
      daftarBuah[currentIndex].hargaTotal = hargaTotal
      daftarBuah[currentIndex].beratTotal = beratTotal
    }
    //kosong
    setCurrentIndex(-1)
    setInputBuah({ nama: "", hargaTotal: '', beratTotal: '' })
  }


  return (
    <>
      <h1>Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga Total</th>
            <th>Berat Total</th>
            <th>Harga per kg</th>
            <th>aksi</th>
          </tr>
        </thead>
        <tbody>
          {daftarBuah !== null && (
            <>
              {daftarBuah.map((e, index, arr) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{e.nama}</td>
                    <td>{e.hargaTotal}</td>
                    <td>{e.beratTotal}</td>
                    <td>{e.hargaTotal / e.beratTotal}</td>
                    <td>
                      <button onClick={handleEdit} value={index}>Edit</button>
                      <button onClick={handleDelete} value={index}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </>
          )}

        </tbody>
      </table><br /><br />
      <h1>Form Daftar Harga Buah</h1>
      <div className="form">
        <form onSubmit={handleSubmit} method="POST">
          <div className="row">
            <label>Nama </label>
            <input type="text" value={inputBuah.nama} name="nama" onChange={handleChange} /><br />
          </div>
          <div className="row">
            <label>Harga Total:</label>
            <input type="number" value={inputBuah.hargaTotal} name="hargaTotal" onChange={handleChange} /><br />
          </div>
          <div className="row">
            <label>Berat Total</label>
            <input type="number" value={inputBuah.beratTotal} name="beratTotal" onChange={handleChange} /><br />
          </div>
          <div className="submit">
            <input type="submit" value="submit" />
          </div>
        </form>
      </div>

    </>
  )
}

export default Tugas11