import React, { useEffect, useState } from "react"
// import '../tugas-11/tugas11.css';
import axios from "axios"

const Tugas12 = () => {

  const [nilaiMahasiswa, setNilaiMahasiswa] = useState([])
  const [inputNilaiMahasiswa, setInputNilaiMahasiswa] = useState({name:"", course: "", score: ''})

  useEffect(() => {
    const fetchData = async () => {
      let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
      setNilaiMahasiswa(result.data.map(e => {
        return { id: e.id, name: e.name, course: e.course, score: e.score }
      }))
      // console.log(result)
      // console.log(nilaiMahasiswa)
    }
    fetchData()
  }, [])

  const inputIndeksNilai = (i) => {
    let indeks = [{}];
    nilaiMahasiswa.map((e, index) => {
      if (e.score >= 80) {
        indeks[index] = 'A';
      }
      else if (e.score >= 70 && e.score < 80) {
        indeks[index] = 'B';
      }
      else if (e.score >= 60 && e.score < 70) {
        indeks[index] = 'C';
      }
      else if (e.score >= 50 && e.score < 60) {
        indeks[index] = 'D';
      }
      else if (e.score < 50) {
        indeks[index] = 'E';
      }
    })
    return indeks[i];
    // console.log({nilaiMahasiswa})
    // setNilaiMahasiswa({...nilaiMahasiswa.indeksNilai}, indeks)
  }

  const [currentIndex, setCurrentIndex] = useState(-1)

  const handleEdit = (event) => {
    let id = event.target.value
    axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
      .then(res => {
        let newData = res.data
        setInputNilaiMahasiswa(newData)
        setCurrentIndex(newData.id)
      })
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    let deletedItem = nilaiMahasiswa[index]
    axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${index}`)
      .then(() => {
        let newData = nilaiMahasiswa.filter(el => { return el.id !== index })
        setNilaiMahasiswa(newData)
      })
  }

  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name

    setInputNilaiMahasiswa({ ...inputNilaiMahasiswa, [name]: value })
  }


  const handleSubmit = (event) => {
    event.preventDefault()

    //tambah
    if (currentIndex < 0 ) {
      axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {
        name: inputNilaiMahasiswa.name,
        course: inputNilaiMahasiswa.course,
        score: inputNilaiMahasiswa.score
      }).then(res => {
        let data = res.data
        setNilaiMahasiswa([...nilaiMahasiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
      })
    }
    //edit
    else if (currentIndex >= 0 ) {
      axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, {
        name: inputNilaiMahasiswa.name,
        course: inputNilaiMahasiswa.course,
        score: inputNilaiMahasiswa.score
      })
        .then(() => {
          let updateData = nilaiMahasiswa.find(e => e.id === currentIndex)
          // console.log(updateData)
          updateData.name = inputNilaiMahasiswa.name
          updateData.course = inputNilaiMahasiswa.course
          updateData.score = inputNilaiMahasiswa.score
          setNilaiMahasiswa([...nilaiMahasiswa])
          console.log(updateData)
        })
    }
    //kosong
    setCurrentIndex(-1)
    setInputNilaiMahasiswa({name:"", course:'',score:''})
    // inputNilaiMahasiswa.name=""
    // inputNilaiMahasiswa.course=""
    // inputNilaiMahasiswa.score=""

    // console.log(inputNilaiMahasiswa)

  }


  return (
    <>
      <h1>Daftar Nilai Mahasiswa</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Mata Kuliah</th>
            <th>Nilai</th>
            <th>Indeks Nilai</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {nilaiMahasiswa !== null && (
            <>
              {nilaiMahasiswa.map((e, index) => {
                return (
                  <tr key={index}>
                    <td>{e.id}</td>
                    <td>{e.name}</td>
                    <td>{e.course}</td>
                    <td>{e.score}</td>
                    <td>{inputIndeksNilai(index)}</td>
                    <td>
                      <button onClick={handleEdit} value={e.id}>Edit</button>
                      <button onClick={handleDelete} value={e.id}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </>
          )}

        </tbody>
      </table><br /><br />
      <h1>Form Nilai Mahasiswa</h1>
      <div className="form">
        <form onSubmit={handleSubmit} method="POST">
          <div className="row">
            <label>Nama </label>
            <input type="text" value={inputNilaiMahasiswa.name} name="name" onChange={handleChange} required/><br />
          </div>
          <div className="row">
            <label>Mata Kuliah:</label>
            <input type="text" value={inputNilaiMahasiswa.course} name="course" onChange={handleChange} required/><br />
          </div>
          <div className="row">
            <label>Nilai</label>
            <input type="number" value={inputNilaiMahasiswa.score} name="score" onChange={handleChange} min="0" max="100" required/><br />
          </div>
          <div className="submit">
            <input type="submit" value="submit" />
          </div>
        </form>
      </div>

    </>
  )
}

export default Tugas12