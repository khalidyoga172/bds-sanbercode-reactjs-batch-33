import React,{useContext} from "react"
import { NilaiContext } from "./nilaiMahasiswaContext"

const NilaiMahasiswaForm = () => {

  const {inputNilaiMahasiswa,functions} = useContext(NilaiContext)
  const {functionHandleChange,functionHandleSubmit}= functions

  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    functionHandleChange(name,value)
  }


  const handleSubmit = (event) => {
    event.preventDefault()
    functionHandleSubmit()
  }

  return (
    <>
      <h1>Form Nilai Mahasiswa</h1>
      <div className="form">
        <form onSubmit={handleSubmit} method="POST">
          <div className="row">
            <label>Nama </label>
            <input type="text" value={inputNilaiMahasiswa.name} name="name" onChange={handleChange} required /><br />
          </div>
          <div className="row">
            <label>Mata Kuliah:</label>
            <input type="text" value={inputNilaiMahasiswa.course} name="course" onChange={handleChange} required /><br />
          </div>
          <div className="row">
            <label>Nilai</label>
            <input type="number" value={inputNilaiMahasiswa.score} name="score" onChange={handleChange} min="0" max="100" required /><br />
          </div>
          <div className="submit">
            <input type="submit" value="submit" />
          </div>
        </form>
      </div>
    </>
  )
}
export default NilaiMahasiswaForm