import React from "react"
import { NilaiProvider } from "./nilaiMahasiswaContext"
import NilaiMahasiswaForm from "./nilaiMahasiswaForm"
import NilaiMahasiswaTabel from "./nilaiMahasiswaTabel"

const NilaiMahasiswa = () =>{
  return(
    <NilaiProvider>
      <NilaiMahasiswaTabel/>
      <NilaiMahasiswaForm/>
    </NilaiProvider>
  )
}
export default NilaiMahasiswa
