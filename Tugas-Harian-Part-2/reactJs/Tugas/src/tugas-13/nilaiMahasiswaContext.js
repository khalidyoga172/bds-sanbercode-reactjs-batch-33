import React, { useState, createContext, useContext } from "react";
import axios from "axios"
import Cookies from 'js-cookie'
import history, { useHistory } from "react-router-dom"
export const NilaiContext = createContext();

export const NilaiProvider = props => {
  const [nilaiMahasiswa, setNilaiMahasiswa] = useState([])
  const [inputNilaiMahasiswa, setInputNilaiMahasiswa] = useState({ name: "", course: "", score: '' })
  const [currentIndex, setCurrentIndex] = useState(-1)
  const [darkMode, setDarkMode] = useState("bg-white bg-white-800  shadow py-4");
  const [userData, setUserdata] = useState({ nama: "", email: "", password: "" })
  let history = useHistory()

  const fetchData = async () => {
    let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
    setNilaiMahasiswa(result.data.map(e => {
      return { id: e.id, name: e.name, course: e.course, score: e.score }
    }))
  }
  const functionIndeksNilai = (e) => {
    if (e.score >= 80) {
      return 'A';
    }
    else if (e.score >= 70 && e.score < 80) {
      return 'B';
    }
    else if (e.score >= 60 && e.score < 70) {
      return 'C';
    }
    else if (e.score >= 50 && e.score < 60) {
      return 'D';
    }
    else if (e.score < 50) {
      return 'E';
    }
  }
  const functionHandleEdit = (id) => {
    console.log(id)
    history.push(`tugas14/edit/${id}`)
    // axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
    //   .then(res => {
    //     let newData = res.data
    //     setInputNilaiMahasiswa(newData)
    //     setCurrentIndex(newData.id)
    //   })
  }
  const functionHandleDelete = (index) => {
    let deletedItem = nilaiMahasiswa[index]
    axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${index}`)
      .then(() => {
        let newData = nilaiMahasiswa.filter(el => { return el.id !== index })
        setNilaiMahasiswa(newData)
      })
  }
  const functionHandleChange = (name, value) => {
    setInputNilaiMahasiswa({ ...inputNilaiMahasiswa, [name]: value })
  }
  const functionHandleSubmit = () => {
    //tambah
    if (currentIndex < 0) {
      axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {
        name: inputNilaiMahasiswa.name,
        course: inputNilaiMahasiswa.course,
        score: inputNilaiMahasiswa.score
      }).then(res => {
        let data = res.data
        setNilaiMahasiswa([...nilaiMahasiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
        history.push('/Tugas14')
      })
    }
    //edit
    else if (currentIndex >= 0) {
      axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, {
        name: inputNilaiMahasiswa.name,
        course: inputNilaiMahasiswa.course,
        score: inputNilaiMahasiswa.score
      })
        .then(() => {
          let updateData = nilaiMahasiswa.find(e => e.id === currentIndex)
          // console.log(updateData)
          updateData.name = inputNilaiMahasiswa.name
          updateData.course = inputNilaiMahasiswa.course
          updateData.score = inputNilaiMahasiswa.score
          setNilaiMahasiswa([...nilaiMahasiswa])
          console.log(updateData)
          history.push('/Tugas14')
        })
    }
    //kosong
    setCurrentIndex(-1)
    setInputNilaiMahasiswa({ name: "", course: '', score: '' })

  }

  const handleChangeAuth = (name, value) => {
    setUserdata({ ...userData, [name]: value })
  }

  const handleRegister = (event) => {
    event.preventDefault()
    let { nama, email, password } = userData
    // console.log(nama + email + password)
    axios.post(`https://backendexample.sanbersy.com/api/register`, { name:nama, email:email, password:password })
      .then(()=>{
        setUserdata({ nama: "", email: "", password: "" })
        history.push('/login')
      })
  }
  const handleLogin = (event) => {
    event.preventDefault()
    let { email, password } = userData
    axios.post(`https://backendexample.sanbersy.com/api/user-login`, { email, password })
      .then((res) => {
        let { token } = res.data
        Cookies.set('token', token)
        setUserdata({ nama: "", email: "", password: "" })
        history.push('/')
      })
  }
  const handleLogout=()=>{
    Cookies.remove('token')
    history.push('/login')
    setUserdata({ nama: "", email: "", password: "" })
  }

  const functions = {
    fetchData,
    functionIndeksNilai,
    functionHandleEdit,
    functionHandleDelete,
    functionHandleChange,
    functionHandleSubmit,
    handleChangeAuth,
    handleRegister,
    handleLogin,
    handleLogout
  }

  return (
    <NilaiContext.Provider value={{
      nilaiMahasiswa,
      setNilaiMahasiswa,
      inputNilaiMahasiswa,
      setInputNilaiMahasiswa,
      currentIndex, setCurrentIndex,
      darkMode, setDarkMode,
      userData, setUserdata,
      functions
    }}>
      {props.children}
    </NilaiContext.Provider>
  )
}