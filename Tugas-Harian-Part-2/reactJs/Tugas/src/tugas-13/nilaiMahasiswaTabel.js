import React, { useContext,useEffect } from "react"
import { NilaiContext } from "./nilaiMahasiswaContext"

const NilaiMahasiswaTabel = () => {

  const {nilaiMahasiswa,setNilaiMahasiswa,inputNilaiMahasiswa,setInputNilaiMahasiswa,currentIndex,setCurrentIndex,functions} = useContext(NilaiContext)
  const {fetchData,functionIndeksNilai,functionHandleEdit,functionHandleDelete,functionHandleChange,functionHandleSubmit}= functions

  useEffect(() => {
    fetchData()
  }, [])

  const inputIndeksNilai = (e) => {
    return functionIndeksNilai(e)
  }


  const handleEdit = (event) => {
    let id = event.target.value
    functionHandleEdit(id)
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    functionHandleDelete(index)
    
  }

  

  return (
    <>
      <h1>Daftar Nilai Mahasiswa</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Mata Kuliah</th>
            <th>Nilai</th>
            <th>Indeks Nilai</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {nilaiMahasiswa !== null && (
            <>
              {nilaiMahasiswa.map((e, index) => {
                return (
                  <tr key={index}>
                    <td>{e.id}</td>
                    <td>{e.name}</td>
                    <td>{e.course}</td>
                    <td>{e.score}</td>
                    <td>{inputIndeksNilai(e)}</td>
                    <td>
                      <button onClick={handleEdit} value={e.id}>Edit</button>
                      <button onClick={handleDelete} value={e.id}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </>
          )}

        </tbody>
      </table><br /><br />
      

    </>
  )
}

export default NilaiMahasiswaTabel