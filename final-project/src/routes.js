import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  BrowserRouter
} from "react-router-dom";
import Cookies from 'js-cookie'
import { LokerProvider } from "./context/context";
import LayoutLanding from "./layout/layoutLanding"
import LayoutDashboard from "./layout/layoutDashboard"
import Beranda from "./pages/beranda"
import HomeDashboard from "./pages/homeDashboard"
import LoginForm from "./pages/loginForm"
import RegisterForm from "./pages/registerForm"
import JobList from "./pages/jobList";
import JobDetail from "./pages/jobDetail";
import Profil from "./pages/profil";
import ChangePassword from "./pages/changePassword";
import JobForm from "./component/jobForm";
// import List from "./list"

const LoginRoute = ({ ...props }) => {

  if (Cookies.get('token') !== undefined) {
    return <Route {...props} />
  } else if (Cookies.get('token') === undefined) {
    return <Redirect to="/login" />
  }

}

const RouterLoker = () => {
  return (
    <BrowserRouter>
      <LokerProvider>
        <Switch>
          <Route path="/" exact>
            <LayoutLanding>
              <Beranda />
            </LayoutLanding>
          </Route>
          <Route path="/list-job-vacancy" exact>
            <LayoutLanding>
              <JobList />
            </LayoutLanding>
          </Route>
          <Route path="/login" exact>
            <LayoutLanding>
              <LoginForm/>
            </LayoutLanding>
          </Route>
          <Route path="/register" exact>
            <LayoutLanding>
              <RegisterForm/>
            </LayoutLanding>
          </Route>

          <LoginRoute path="/dashboard" exact>
            <LayoutDashboard>
              <HomeDashboard />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/list-job-vacancy" exact>
            <LayoutDashboard>
              <JobList />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/job-vacancy/:id" exact>
            <LayoutDashboard>
              <JobDetail />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/profil" exact>
            <LayoutDashboard>
              <Profil />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/password" exact>
            <LayoutDashboard>
              <ChangePassword />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/create" exact>
            <LayoutDashboard>
              <JobForm />
            </LayoutDashboard>
          </LoginRoute>
          <LoginRoute path="/dashboard/job-vacancy/edit/:id" exact>
            <LayoutDashboard>
              <JobForm />
            </LayoutDashboard>
          </LoginRoute>

          {/*      
          <LoginRoute path="/side" exact component={Sidebar}/>
          <LoginRoute path="/change-password" exact component={Sidebar}/>
          <LoginRoute path="/job-vacancy/edit" exact component={Sidebar}/>
          <LoginRoute path="/job-vacancy/create" exact component={Sidebar}/> */}
        </Switch>
      </LokerProvider>
    </BrowserRouter>
  )
}
export default RouterLoker