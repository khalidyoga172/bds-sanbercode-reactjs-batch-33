import React, { useState, createContext } from "react"
import Cookies from 'js-cookie'
import axios from 'axios'
import history, { useHistory } from "react-router-dom"

export const LokerContext = createContext();
export const LokerProvider = props => {
  const [loker, setLoker] = useState([])
  const [userData, setUserdata] = useState({ name: "", image_url: "", email: "", password: "" })
  const [inputLoker, setInputLoker] = useState({
    id: -1,
    title: '',
    job_description: '',
    job_qualification: '',
    job_type: '',
    job_tenure: '',
    job_status: 1,
    company_name: '',
    company_image_url: '',
    company_city: '',
    salary_min: 0,
    salary_max: 0
  })
  let history = useHistory()


  const fetchData = async () => {
    let result = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
    setLoker(result.data.data.map(e => {
      return {
        id: e.id,
        title: e.title,
        job_description: e.job_description,
        job_qualification: e.job_qualification,
        job_type: e.job_type,
        job_tenure: e.job_tenure,
        job_status: e.job_status,
        company_name: e.company_name,
        company_image_url: e.company_image_url,
        company_city: e.company_city,
        salary_min: e.salary_min,
        salary_max: e.salary_max
      }
    }))
  }
  const handleChangeAuth = (name, value) => {
    setUserdata({ ...userData, [name]: value })
  }
  const handleRegister = (event) => {
    event.preventDefault()
    let { name, image_url, email, password } = userData
    axios.post(`https://dev-example.sanbercloud.com/api/register `, { name: name, image_url: image_url, email: email, password: password })
      .then(() => {
        setUserdata({ name: "", image_url: "", email: "", password: "" })
        history.push('/login')
      })
  }
  const handleLogin = (event) => {
    event.preventDefault()
    let { email, password } = userData
    axios.post(`https://dev-example.sanbercloud.com/api/login `, { email, password })
      .then((res) => {
        // setUserdata(res.data.user)
        // console.log(userData)
        let { token } = res.data
        Cookies.set('token', token)
        Cookies.set('name',res.data.user.name)
        Cookies.set('email',res.data.user.email)
        Cookies.set('image_url',res.data.user.image_url)
        Cookies.set('password',res.data.user.password)
        console.log(res.data)

        // setUserdata({ nama: "", email: "", password: "" })
        history.push('/dashboard')
      })
  }
  const handleLogout = () => {
    setUserdata({ nama: "", email: "", password: "" })
    Cookies.remove('token')
    history.push('/')
  }
  const formatRupiah = (angka) => {
    let data = parseInt(angka)
    let sdata = data.toString()
    let i = 0

    do {
      data = data / 1000
      sdata = sdata.slice(0, sdata.length - (3 * (i + 1) + i)) + "." + sdata.slice(sdata.length - (3 * (i + 1) + i));
      i++;
    }
    while (data / 1000 >= 1)

    return "Rp." + sdata
  }



  const functions = {
    handleChangeAuth,
    fetchData,
    handleRegister,
    handleLogin,
    handleLogout,
    formatRupiah
  }

  return (
    <LokerContext.Provider value={{
      loker, setLoker,
      userData, setUserdata,
      inputLoker, setInputLoker,
      functions
    }}>
      {props.children}
    </LokerContext.Provider>
  )
}