import React, { useContext, useEffect, useState } from "react"
import Cookies from "js-cookie";
import axios from "axios"
import history, { useHistory } from "react-router-dom"
import { LokerContext } from "../context/context"

const JobList = () => {
  const { functions, loker, setLoker } = useContext(LokerContext)
  const { fetchData, formatRupiah } = functions
  const [searchValue, setSearchValue] = useState('')
  const [showDetails, setShowDetails] = useState(false)
  const [currentIndex, setCurrentIndex] = useState(-1)
  const [filterValue, setFilterValue] = useState({ job_description: "", company_city: "", job_status: '' })
  let history = useHistory()

  useEffect(() => {
    fetchData()
  }, [])


  const handleDetail = (event) => {
    let id = event.target.value
    history.push(`/dashboard/job-vacancy/${id}`)
  }
  const editHandle = (event) => {
    let id = event.target.value
    history.push(`/dashboard/job-vacancy/edit/${id}`)
  }
  const deleteHandle = (event) => {
    let id = event.target.value
    axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`, {
      headers: { "Authorization": "Bearer " + Cookies.get('token') }
    })
      .then(() => {
        fetchData()
      })
  }
  const ChangeHandle = (event) => {
    let value = event.target.value
    let name = event.target.name
    console.log()
    setFilterValue({ ...filterValue, [name]: value })
  }
  const searchChangeHandle = (event) => {
    let value = event.target.value
    console.log()
    setSearchValue(value)
  }
  const searchSubmitHandle = async (event) => {
    event.preventDefault()
    let result = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
    let resultData = result.data.data
    let filterData = resultData.filter((e) => {
      return Object.values(e).join(" ").toLowerCase().includes(searchValue.toLowerCase())
    })
    setLoker([...filterData])
    setSearchValue('')
  }
  const filterSubmitHandle = async (event) => {
    event.preventDefault()
    let result = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)

    let resultData = result.data.data
    let filterData = resultData.filter((e) => {
      return e.job_status === Number(filterValue.job_status) || e.job_description === filterValue.job_description || e.company_city === filterValue.company_city
    })
    setFilterValue({ job_description: "", company_city: "", job_status: '' })
    setLoker([...filterData])
  }

  const clearFilter = () => {
    setFilterValue({ job_description: "", company_city: "", job_status: '' })
    fetchData()
  }

  return (
    <>
      <main className="bg-gray-100 dark:bg-gray-800 h-screen  relative">
        <div className="mt-5 flex items-start justify-between">
          <div className="flex flex-col w-full md:space-y-4">
            <div className="overflow-auto h-screen pb-24 px-4 md:px-6">
              <div className="flex items-center space-x-4">
                <form onSubmit={searchSubmitHandle} method="POST" className="flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 ">
                  <div className=" relative ">
                    <input type="text" onChange={searchChangeHandle} value={searchValue} id="&quot;form-subscribe-searchValue" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Cari Lowongan" />
                  </div>
                  <button value="Submit" className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-blue-600 rounded-lg shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-blue-200" type="submit">
                    search
                  </button>
                </form>

                <form onSubmit={filterSubmitHandle} method="POST" className="flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
                  <div className=" relative ">
                    <input onChange={ChangeHandle} value={filterValue.job_description} type="text" id="job_description" name="job_description" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="posisi" />
                  </div>
                  <div className=" relative ">
                    <input onChange={ChangeHandle} value={filterValue.company_city} type="text" id="company_city" name="company_city" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="lokasi" />
                  </div>
                  <div className=" relative ">
                    <input onChange={ChangeHandle} value={filterValue.job_status} type="number" id="job_status" name="job_status" min='0' max='1' className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-18 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="1/0" />
                  </div>
                  <button value="Submit" className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-blue-600 rounded-lg shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-blue-200" type="submit">
                    set filter
                  </button>
                </form>
              </div>
              <button onClick={clearFilter} value="Submit" className="mt-2 flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-red-600 rounded-lg shadow-md hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2 focus:ring-offset-red-200" >
                clear filter
              </button>
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 my-4">

                {loker !== null && (
                  <>
                    {loker.map((e, index) => {
                      return (
                        <div key={index} className="w-full">
                          <div className="shadow-lg px-4 py-6 w-full bg-white dark:bg-gray-700 relative">
                            <p className="text-xl text-slate-800 font-bold font-mono dark:text-white font-semibold">
                              {e.title} ({e.job_tenure})
                            </p>
                            <div className="flex items-end space-x-2 my-6">
                              <img className="h-40" src={e.company_image_url} />
                            </div>
                            <div className="dark:text-white">
                              <div className="text-xl flex items-center pb-2 mb-2 text-sm sm:space-x-12  justify-between border-b border-gray-200">
                                {e.company_name}
                              </div>
                              <div className="flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                                {e.company_city}
                              </div>
                              <div className="flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                                {formatRupiah(e.salary_min)} - {formatRupiah(e.salary_max)}
                              </div>
                              <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                                <div className={e.job_status ? "text-lime-500" : "text-red-600"}>
                                  {e.job_status ? "available" : "closed"}

                                </div>
                              </div>
                              {!Cookies.get('token') &&
                                <button onClick={() => {
                                  console.log(showDetails)

                                  showDetails ? setShowDetails(false) : setShowDetails(true)
                                  showDetails ? setCurrentIndex(-1) : setCurrentIndex(index)
                                  console.log(currentIndex)
                                }} className={showDetails && currentIndex === index ? "hidden" : "mt-2 text-sky-400 items-center text-sm space-x-12 md:space-x-24 justify-between"}>
                                  . . . detail
                                </button>
                              }

                              {Cookies.get('token') &&
                                <button onClick={handleDetail} value={e.id} className={showDetails && currentIndex === index ? "hidden" : "mt-2 text-sky-400 items-center text-sm space-x-12 md:space-x-24 justify-between"}>
                                  detail
                                </button>
                              }


                              <div className={showDetails && currentIndex === index ? "" : "hidden"}>
                                <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                                  {e.job_type}
                                </div>
                                <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                                  {e.job_description}
                                </div>
                                <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                                  {e.job_qualification}
                                </div>
                              </div>

                              <button onClick={() => {
                                showDetails ? setShowDetails(false) : setShowDetails(true)
                                showDetails ? setCurrentIndex(-1) : setCurrentIndex(index)
                              }} className={showDetails && currentIndex === index ? "mt-2 text-sky-400 items-center text-sm space-x-12 md:space-x-24 justify-between" : "hidden"}>
                                . . . less
                              </button>

                              {Cookies.get('token') &&
                                <div className="mt-5 flex items-center text-sm space-x-2 justify-between">
                                  <button onClick={editHandle} value={e.id} type="button" className="py-2 px-4  bg-green-600 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                    Edit
                                  </button>
                                  <button onClick={deleteHandle} value={e.id} type="button" className="py-2 px-4  bg-red-600 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                    Delete
                                  </button>
                                </div>
                              }
                            </div>
                          </div>
                        </div>


                      )
                    })}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>

    </>
  )
}
export default JobList