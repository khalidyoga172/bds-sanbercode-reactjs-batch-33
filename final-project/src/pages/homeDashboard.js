import React, { useContext, useEffect } from 'react';
import { LokerContext } from '../context/context';
import Cookies from 'js-cookie'

const HomeDashboard = () => {
  const { functions, loker, setLoker, userData, setUserData } = useContext(LokerContext)
  const { fetchData } = functions

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <>
      <main className="grow dark:bg-gray-800  relative">

        <div className="w-full md:space-y-4">

          <div className=" pb-24 px-4 md:px-6">
            <div className='flex mt-5 item-center font-semibold text-gray-800 dark:text-white'>
              <h1 className="text-4xl">
                Welcome Back,
              </h1>
              <div className="pt-4 pl-2">

                {Cookies.get('name') ? Cookies.get('name') : Cookies.get('email')}
              </div>
            </div>

            <h2 className="text-md text-gray-400">
              Here's what's happening today.
            </h2>

            <div className="shadow-lg px-4 py-6 w-48 bg-white dark:bg-gray-700 relative">
              <p className="text-sm w-max text-gray-700 dark:text-white font-semibold border-b border-gray-200">
                Jumlah Loker
              </p>
              <div className="flex items-end space-x-2 my-6">
                <p className="text-5xl text-black dark:text-white font-bold">
                  {loker.length}
                </p>

              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default HomeDashboard;