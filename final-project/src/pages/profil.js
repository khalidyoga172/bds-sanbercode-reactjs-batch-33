import React, { useContext } from 'react';
import { LokerContext } from '../context/context';
import Cookies from 'js-cookie'


const Profil = () => {
  const { functions, loker, setLoker, userData, setUserData } = useContext(LokerContext)
  const { fetchData, handleChangeAuth, handleRegister } = functions

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value
    handleChangeAuth(name, value)
  }
  return (
    <>
      <main className="grow dark:bg-gray-800  relative">

        <div className="w-full md:space-y-4">

          <div className="mt-6 pb-24 px-4 md:px-6">


            <div className="flex shadow-lg px-4 py-6 bg-white dark:bg-gray-700 relative">
              <p className="text-sm w-max text-gray-700 dark:text-white font-semibold border-b border-gray-200">
                <img className='w-72' src={Cookies.get('image_url')} />
              </p>
              <form >
                <div className="ml-10 flex flex-col block items-end space-x-2 my-6">
                  <p className="mt-5 text-black dark:text-white ">
                    <input  value={Cookies.get('image_url')} required type="text" id="image_url" className="rounded-lg border-transparent flex-1 appearance-none border border-gray-700 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" name="name" />
                  </p>
                  <p className="mt-5 text-black dark:text-white ">
                    <input  value={Cookies.get('name')} required type="text" id="name" className="rounded-lg border-transparent flex-1 appearance-none border border-gray-700 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" name="name" />
                  </p>
                  <p className="mt-5 text-black dark:text-white ">
                    <input  value={Cookies.get('email')} required type="text" id="email" className="rounded-lg border-transparent flex-1 appearance-none border border-gray-700 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" name="name" />
                  </p>
                </div>
              </form>

            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default Profil;