import React, { useState } from 'react';
import Cookies from 'js-cookie'
import axios from "axios"
import history, { useHistory } from "react-router-dom"

const ChangePassword = () => {

  const [dataNewPassword, setDataNewPassword] = useState({ currentPassword: "", newPassword: "", confirmNewPassword: "" })
  const [sama, setSama] = useState(true)
  let history = useHistory()

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value
    setDataNewPassword({ ...dataNewPassword, [name]: value })
    setSama(true)
  }
  const changePasswordSubmit = (event) => {
    event.preventDefault()
    if (dataNewPassword.newPassword === dataNewPassword.confirmNewPassword) {
      axios.post(`https://dev-example.sanbercloud.com/api/change-password `, {
        current_password: dataNewPassword.currentPassword,
        new_password: dataNewPassword.newPassword,
        new_confirm_password: dataNewPassword.confirmNewPassword
      }, {
        headers: {
          "Authorization": "Bearer " + Cookies.get('token')
        }
      }

      )
        .then(() => {
          setDataNewPassword({ currentPassword: "", dataNewPassword: "", dataNewPassword: "" })
          history.push('/dashboard')
        })
    }
    else if (dataNewPassword.newPassword !== dataNewPassword.confirmNewPassword) {
      setSama(false)
    }

  }

  return (
    <>
      <div className="h-full bg-stone-200  p-10">
        <div className="w-96 sm:w-7/12 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <form onSubmit={changePasswordSubmit} method="POST" id="form1">
            <div className=" flex mt-2">
              <label htmlFor="name" className="text-gray-700 block">
                Password
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={dataNewPassword.currentPassword} required type="password" id="currentPassword" name="currentPassword" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className="flex mt-2">
              <label htmlFor="email" className="text-gray-700 block">
                New Password
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={dataNewPassword.newPass} required type="password" id="NewPassword" name="newPassword" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                Confirm New Password
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={dataNewPassword.confirmNewPass} required minlength="8" type="password" id="confirmNewPassword" name="confirmNewPassword" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className={sama ? "hidden" : ""}>
              <div className='text-red-700 text-sm'>
                password tidak sama
              </div>
            </div>
            <div className="submit">
              <button form="form1" value="Submit" className="mt-10 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg " >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div >
    </>
  );
}

export default ChangePassword;