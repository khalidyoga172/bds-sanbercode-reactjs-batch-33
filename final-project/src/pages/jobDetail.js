import React, { useContext, useEffect } from 'react';
import axios from "axios"
import { useParams } from "react-router-dom"
import { LokerContext } from '../context/context';

const JobDetail = () => {
  const { functions, loker, setLoker, userData, setUserData, inputLoker, setInputLoker } = useContext(LokerContext)
  const { fetchData, formatRupiah } = functions
  let { id } = useParams();

  useEffect(() => {
    axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`)
      .then(res => {
        let newData = res.data
        setInputLoker(newData)
      })
      
  }, [])

  return (
    <>
      <main className="grow dark:bg-gray-800  relative">

        <div className="w-full md:space-y-4">

          <div className=" pb-24 px-4 md:px-6">

            <div className="w-full">
              <div className="shadow-lg px-4 py-6 w-full bg-white dark:bg-gray-700 relative">
                <p className="text-xl text-slate-800 font-bold font-mono dark:text-white font-semibold">
                  {inputLoker.title} ({inputLoker.job_tenure})
                </p>
                <div className="flex items-end space-x-2 my-6">
                  <img className="h-40" src={inputLoker.company_image_url} />
                </div>
                <div className="dark:text-white">
                  <div className="text-xl flex items-center pb-2 mb-2 text-sm sm:space-x-12  justify-between border-b border-gray-200">
                    {inputLoker.company_name}
                  </div>
                  <div className="flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                    {inputLoker.company_city}
                  </div>
                  <div className="flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                    {formatRupiah(inputLoker.salary_min)} - {formatRupiah(inputLoker.salary_max)}
                  </div>
                  <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between">
                    <div className={inputLoker.job_status ? "text-lime-500" : "text-red-600"}>
                      {inputLoker.job_status ? "available" : "closed"}

                    </div>
                  </div>
                  <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                    {inputLoker.job_type}
                  </div>
                  <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                    {inputLoker.job_description}
                  </div>
                  <div className="mt-2 flex items-center text-sm space-x-12 md:space-x-24 justify-between border-t border-gray-200">
                    {inputLoker.job_qualification}
                  </div>



                  <div className="mt-5 flex items-center text-sm space-x-2 justify-between">
                    <button type="button" className="py-2 px-4  bg-green-600 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                      Edit
                    </button>
                    <button type="button" className="py-2 px-4  bg-red-600 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                      Delete
                    </button>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default JobDetail;