import React from 'react';

const Home = () => {
  return (
    <>
      <div id="controls-carousel" className="relative z-0" data-carousel="slide">
        {/* Carousel wrapper */}
        <div className="overflow-hidden relative h-96 rounded-lg text-5xl sm:text-4xl">
          {/* Item 1 */}
          <div className="hidden duration-7 ease-in-out" data-carousel-item>
            <div className="absolute w-2/5 ml-20 font-mono text-slate-800  font-weight: 700 z-10 m-10 ">
              Find your dream jobs with us easily
            </div>
            <img src="https://img.freepik.com/free-photo/indoor-photo-happy-glad-handsome-guy_295783-534.jpg?w=996&t=st=1649406250~exp=1649406850~hmac=857af0c860f2267bfe25ac1cf4055f97324a1f020489967e64a51f93ea846da0" className="block absolute z-0 w-full" alt="..." />
          </div>
          {/* Item 2 */}
          <div className="hidden duration-700 ease-in-out" data-carousel-item="active">
            <img src="https://www.trevenacross.co.uk/wp-content/uploads/2019/03/Job-Vacancy.jpg" className="block absolute z-0 w-full" alt="..." />
          </div>
          {/* Item 3 */}
          <div className="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="https://www.axis.eu/uploads/_1200x630_crop_center-center_82_none/Findajob_pexels.jpg?mtime=1613751038" className="block absolute z-0 w-full" alt="..." />
          </div>

        </div>
        {/* Slider controls */}
        <button type="button" className="flex absolute top-0 left-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none" data-carousel-prev>
          <span className="inline-flex justify-center items-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg className="w-6 h-6 text-white dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" /></svg>
            <span className="hidden">Previous</span>
          </span>
        </button>
        <button type="button" className="flex absolute top-0 right-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none" data-carousel-next>
          <span className="inline-flex justify-center items-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg className="w-6 h-6 text-white dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" /></svg>
            <span className="hidden">Next</span>
          </span>
        </button>
      </div>


      <main className="bg-gray-100 dark:bg-gray-800 h-screen overflow-hidden relative">
        <div className="flex items-start justify-between">
          
          <div className="flex flex-col w-full md:space-y-4">
            
            <div className="overflow-auto h-screen pb-24 px-4 md:px-6">
              <h1 className="text-4xl font-semibold text-gray-800 dark:text-white">
                Good afternoon
              </h1>
              <h2 className="text-md text-gray-400">
                Today is the day, if its not, there is another day
              </h2>
              
              
            </div>
          </div>
        </div>
      </main>




    </>
  );
}

export default Home;