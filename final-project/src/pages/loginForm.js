import React, { useContext } from 'react';
import { LokerContext } from '../context/context';

const LoginForm = () => {
  const { functions, loker, setLoker, userData, setUserData } = useContext(LokerContext)
  const { fetchData, handleChangeAuth, handleLogin } = functions

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value
    handleChangeAuth(name, value)
  }

  return ( 
    <>
      <div className="bg-stone-200  p-10">
        <div className="w-96 sm:w-7/12 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <form onSubmit={handleLogin} method="POST" id="form1">
            <div className=" relative mt-2">
              <label htmlFor="email" className="text-gray-700 block">
                Email
              </label>
              <input onChange={handleChange} value={userData.email} required type="text" id="email" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="email" placeholder="email" />
            </div>
            <div className=" relative mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                Password
              </label>
              <input onChange={handleChange} value={userData.password} required minlength="8" type="password" id="password" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="password" />
            </div>
            <div className="submit">
              <button form="form1" value="Submit" className="mt-10 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div >
    </>
   );
}
 
export default LoginForm;