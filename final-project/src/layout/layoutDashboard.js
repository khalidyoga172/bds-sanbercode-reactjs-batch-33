import React from 'react';
import Sidebar from '../component/sidebar'
import NavDashboard from '../component/navDashboard'
import FooterDashboard from '../component/footerDashboard';

const LayoutDashboard = (props) => {
  return (
    <>
      <div className="flex">
        <Sidebar />
        <div className="bg-gray-100 flex justify-between flex-col w-full h-screen">
          <NavDashboard/>
          {props.children}
        </div>
      </div>
    </>
  );
}

export default LayoutDashboard;
