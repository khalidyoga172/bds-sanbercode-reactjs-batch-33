import React, { useState } from 'react';
import NavLandingPage from '../component/navLandingPage';
import FooterLandingPage from '../component/footerLandingPage';

const LayoutLanding = (props) => {



  return (
    <>
      <div className="bg-gray-100 flex justify-between flex-col w-full h-screen">
        <NavLandingPage />
        {props.children}
        <FooterLandingPage />
      </div>
    </>
  )
}

export default LayoutLanding;