import React, { useContext, useEffect } from 'react';
import { LokerContext } from '../context/context';
import Cookies from 'js-cookie'
import axios from "axios"
import history, { useHistory, useParams } from "react-router-dom"


const JobForm = () => {
  const { inputLoker, setInputLoker } = useContext(LokerContext)
  let history = useHistory()
  let { id } = useParams();


  useEffect(() => {
    axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`)
      .then(res => {
        let newData = res.data
        setInputLoker(newData)
      })

  }, [])

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value
    setInputLoker({ ...inputLoker, [name]: value })
  }

  const submitJobHandle = (event) => {
    event.preventDefault()

    if (inputLoker.id >= 0) {
      axios.put(`https://dev-example.sanbercloud.com/api/job-vacancy/${id} `, {
        id: inputLoker.id,
        title: inputLoker.title,
        job_description: inputLoker.job_description,
        job_qualification: inputLoker.job_qualification,
        job_type: inputLoker.job_type,
        job_tenure: inputLoker.job_tenure,
        job_status: inputLoker.job_status,
        company_name: inputLoker.company_name,
        company_image_url: inputLoker.company_image_url,
        company_city: inputLoker.company_city,
        salary_min: inputLoker.salary_min,
        salary_max: inputLoker.salary_max
      }, {
        headers: {
          "Authorization": "Bearer " + Cookies.get('token')
        }
      }).then(() => {
        setInputLoker({
          id: -1,
          title: '',
          job_description: '',
          job_qualification: '',
          job_type: '',
          job_tenure: '',
          job_status: 1,
          company_name: '',
          company_image_url: '',
          company_city: '',
          salary_min: 0,
          salary_max: 0
        })
        history.push('/dashboard')
      })
    }
    else {
      axios.post(`https://dev-example.sanbercloud.com/api/job-vacancy `, {
        id: inputLoker.id,
        title: inputLoker.title,
        job_description: inputLoker.job_description,
        job_qualification: inputLoker.job_qualification,
        job_type: inputLoker.job_type,
        job_tenure: inputLoker.job_tenure,
        job_status: inputLoker.job_status,
        company_name: inputLoker.company_name,
        company_image_url: inputLoker.company_image_url,
        company_city: inputLoker.company_city,
        salary_min: inputLoker.salary_min,
        salary_max: inputLoker.salary_max
      }, {
        headers: {
          "Authorization": "Bearer " + Cookies.get('token')
        }
      }).then(() => {
        setInputLoker({
          id: -1,
          title: '',
          job_description: '',
          job_qualification: '',
          job_type: '',
          job_tenure: '',
          job_status: 1,
          company_name: '',
          company_image_url: '',
          company_city: '',
          salary_min: 0,
          salary_max: 0
        })
        history.push('/dashboard')
      })
    }
  }



  return (
    <>
      <div className="bg-stone-200  p-10">
        <div className="w-96 sm:w-10/12 p-5 my-0 mx-auto dark:bg-gray-800 bg-gray-50 relative">
          <form onSubmit={submitJobHandle} method="POST" id="form1">
            <div className=" flex mt-2">
              <label htmlFor="name" className="text-gray-700 block">
                title
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.title} required type="text" id="title" name="title" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className="flex mt-2">
              <label htmlFor="email" className="text-gray-700 block">
                job_description
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.job_description} required type="text" id="job_description" name="job_description" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                job_qualification
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.job_qualification} required type="text" id="job_qualification" name="job_qualification" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                job_type
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} placeholder="WFO / WFH / . . ." value={inputLoker.job_type} required type="text" id="job_type" name="job_type" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                job_tenure
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} placeholder="kontrak / magang / . . ." value={inputLoker.job_tenure} required type="text" id="job_tenure" name="job_tenure" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                job_status
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.job_status} required type="number" min="0" max="1" id="job_status" name="job_status" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                company_name
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.company_name} required type="text" id="company_name" name="company_name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                company_image_url
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.company_image_url} required type="text" id="company_image_url" name="company_image_url" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">
              <label htmlFor="release_year" className="text-gray-700 block">
                company_city
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <input onChange={handleChange} value={inputLoker.company_city} required type="text" id="company_city" name="company_city" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            <div className=" flex mt-2">

              <label htmlFor="release_year" className="text-gray-700 block">
                range gaji
              </label>
              <span className="text-red-500 required-dot">
                *
              </span>
            </div>
            <div className=" flex mt-2 gap-5">
              <input onChange={handleChange} value={inputLoker.salary_min} required type="number" id="salary_min" name="salary_min" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
              <input onChange={handleChange} value={inputLoker.salary_max} required type="number" id="salary_max" name="salary_max" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" />
            </div>
            <div className="submit">
              <button form="form1" value="Submit" className="mt-10 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg " >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>

    </>
  );
}

export default JobForm;