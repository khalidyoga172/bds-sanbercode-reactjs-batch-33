import React, { useContext, useState } from "react"
import { Link } from "react-router-dom"
import Logo from '../assets/img/RNG.png'
import { LokerContext } from "../context/context"


const NavLandingPage = () => {
  const [isHidden, setIsHidden] = useState(true)
  const [showLogin, setShowLogin] = useState(false)
  const {functions}=useContext(LokerContext)
  const {handleLogin}=functions

  const clickHandle = () => {
    isHidden ? setIsHidden(false) : setIsHidden(true)
  }


  return (
    <>
      <div className="relative">
        <nav className="bg-white dark:bg-gray-800  shadow ">
          <div className="">
            <div className="flex items-center justify-between h-16">
              <div className=" flex items-center">
                <a className="flex-shrink-0" href="/">
                  <img className="ml-5 h-9" src={Logo} alt="Workflow" />
                </a>
                <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
                    <Link to="/list-job-vacancy" className="  text-xl font-medium text-gray-500  hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md" href="/#">
                      Lihat Lowongan
                    </Link>
                  </div>
                </div>
              </div>
              <div className="flex">
                <div className="ml-4 flex items-center md:ml-6">
                  <div className="ml-3 relative">
                    <div className="relative inline-block text-left">
                      <div>
                        <button onClick={() => {
                          showLogin ? setShowLogin(false) : setShowLogin(true)
                        }} type="button" className="  flex items-center justify-center w-full rounded-md  px-4 py-2 text-sm font-medium text-gray-700 dark:text-gray-50 hover:bg-gray-50 dark:hover:bg-gray-500 " id="options-menu">
                          <svg width={20} fill="currentColor" height={20} className="text-gray-800" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1523 1339q-22-155-87.5-257.5t-184.5-118.5q-67 74-159.5 115.5t-195.5 41.5-195.5-41.5-159.5-115.5q-119 16-184.5 118.5t-87.5 257.5q106 150 271 237.5t356 87.5 356-87.5 271-237.5zm-243-699q0-159-112.5-271.5t-271.5-112.5-271.5 112.5-112.5 271.5 112.5 271.5 271.5 112.5 271.5-112.5 112.5-271.5zm512 256q0 182-71 347.5t-190.5 286-285.5 191.5-349 71q-182 0-348-71t-286-191-191-286-71-348 71-348 191-286 286-191 348-71 348 71 286 191 191 286 71 348z">
                            </path>
                          </svg>
                        </button>
                      </div>
                      <div className={showLogin ? "relative z-10" : "hidden "}>
                        <div className="float origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white dark:bg-gray-800 ring-1 ring-black ring-opacity-5">
                          <div className="py-1 " role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                            <span className="flex flex-col">
                              <Link onClick={() => {
                                showLogin ? setShowLogin(false) : setShowLogin(true)
                              }} to="/login" className=" block px-4 py-2 text-md text-gray-700 hover:bg-gray-100 hover:text-gray-900 dark:text-gray-100 dark:hover:text-white dark:hover:bg-gray-600" role="menuitem">
                                <span>
                                  Login
                                </span>
                              </Link>
                              <Link onClick={() => {
                                showLogin ? setShowLogin(false) : setShowLogin(true)
                              }} to="/register" className="block px-4 py-2 text-md text-gray-700 hover:bg-gray-100 hover:text-gray-900 dark:text-gray-100 dark:hover:text-white dark:hover:bg-gray-600">
                                <span>
                                  Register
                                </span>
                              </Link>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="-mr-2 flex md:hidden">
                  <button onClick={clickHandle} className="text-gray-800 dark:text-white hover:text-gray-300 inline-flex items-center justify-center p-2 rounded-md focus:outline-none">
                    <svg width={20} height={20} fill="currentColor" className="h-8 w-8" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                      <path d="M1664 1344v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45z">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>

            </div>
          </div>
          <div className={isHidden ? "hidden md:hidden" : ""}>
            <div className="ml-10 flex items-baseline space-x-4">
              <Link to="/list-job-vacancy" className="text-gray-500 text-xl font-medium  hover:text-gray-800 dark:hover:text-white px-3 py-2 rounded-md">
                Lihat Lowongan
              </Link>
            </div>
          </div>
        </nav>
      </div>
    </>
  )
}
export default NavLandingPage