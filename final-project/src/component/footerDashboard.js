import React from 'react';

const FooterDashboard = () => {
  return (
    <>
      <footer className="bg-gray-200 text-center lg:text-left">
        <div className="text-slate-100 text-center p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
          © 2021 Copyright:
          <a className="text-slate-100" href="https://tailwind-elements.com/">Tailwind Elements</a>
        </div>
      </footer>

    </>
  );
}

export default FooterDashboard;