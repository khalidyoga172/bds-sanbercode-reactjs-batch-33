import Cookies from 'js-cookie';
import React, { useContext, useState } from 'react';
import { Link } from "react-router-dom"
import { LokerContext } from '../context/context';


const NavLandingPage = () => {
  const [showLogout, setShowLogout] = useState(false)
  const { functions, userData } = useContext(LokerContext)
  const { handleLogout } = functions

  return (
    <>
      <div className="relative">
        <nav className="bg-white dark:bg-gray-800  shadow ">
          <div className="max-w-7xl mx-auto pr-1">
            <div className="flex items-center flex-row-reverse h-16">

              <div className="ml-4 flex items-center md:ml-6">
                <div className="ml-3 relative">
                  <div className="relative inline-block text-left">
                    <div>
                      <button onClick={() => {
                        showLogout ? setShowLogout(false) : setShowLogout(true)
                      }} type="button" className="  flex items-center justify-center w-full rounded-md  px-4 py-2 text-sm font-medium text-gray-700 dark:text-gray-50 hover:bg-gray-50 dark:hover:bg-gray-500 " id="options-menu">
                        <div class="sec self-center p-2 pr-1">
                          <img data="picture" class="h-10 w-10 border p-0.5 rounded-full" src={Cookies.get('image_url')} alt="" />
                        </div>
                      </button>
                    </div>
                    <div className={showLogout ? "relative z-10" : "hidden "}>
                      <div className="float origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white dark:bg-gray-800 ring-1 ring-black ring-opacity-5">
                        <div className="py-1 " role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                          <a onClick={handleLogout} className=" block px-4 py-2 text-md text-gray-700 hover:bg-gray-100 hover:text-gray-900 dark:text-gray-100 dark:hover:text-white dark:hover:bg-gray-600" role="menuitem">
                            <span className="flex flex-col">
                              <span>
                                Logout
                              </span>
                            </span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </nav>
      </div>
    </>
  );
}

export default NavLandingPage;