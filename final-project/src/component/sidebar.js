import React, { useState } from 'react';
import { Link } from "react-router-dom"
import CreateIcon from '../assets/img/create-icon.jpg'
import Profile from '../assets/img/account.jpg'
import Lock from '../assets/img/lock.png'
import Arrow from '../assets/img/arrow.png'

const Sidebar = () => {
  const [showSidebar, setShowSidebar] = useState(false)
  return (
    <>
      <div className="h-screen hidden sm:block shadow-lg relative w-80">
        <div className="bg-white h-full rounded-2xl dark:bg-gray-700">
          <div className="flex items-center justify-center pt-6">

          </div>
          <nav className="mt-6">
            <div>
              <Link to="/dashboard" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                <span className="text-left">
                  <svg width={20} height={20} fill="currentColor" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1070 1178l306-564h-654l-306 564h654zm722-282q0 182-71 348t-191 286-286 191-348 71-348-71-286-191-191-286-71-348 71-348 191-286 286-191 348-71 348 71 286 191 191 286 71 348z">
                    </path>
                  </svg>
                </span>
                <span className="mx-4 text-sm font-normal">
                  Dashboard
                </span>
              </Link>
              <Link to="/dashboard/list-job-vacancy" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                <span className="text-left">
                  <svg width={20} height={20} fill="currentColor" className="m-auto" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1024 1131q0-64-9-117.5t-29.5-103-60.5-78-97-28.5q-6 4-30 18t-37.5 21.5-35.5 17.5-43 14.5-42 4.5-42-4.5-43-14.5-35.5-17.5-37.5-21.5-30-18q-57 0-97 28.5t-60.5 78-29.5 103-9 117.5 37 106.5 91 42.5h512q54 0 91-42.5t37-106.5zm-157-520q0-94-66.5-160.5t-160.5-66.5-160.5 66.5-66.5 160.5 66.5 160.5 160.5 66.5 160.5-66.5 66.5-160.5zm925 509v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm0-260v-56q0-15-10.5-25.5t-25.5-10.5h-568q-15 0-25.5 10.5t-10.5 25.5v56q0 15 10.5 25.5t25.5 10.5h568q15 0 25.5-10.5t10.5-25.5zm0-252v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm256-320v1216q0 66-47 113t-113 47h-352v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-768v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-352q-66 0-113-47t-47-113v-1216q0-66 47-113t113-47h1728q66 0 113 47t47 113z">
                    </path>
                  </svg>
                </span>
                <span className="mx-4 text-sm font-normal">
                  Job List
                </span>
              </Link>
              <Link to="/dashboard/create" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                <span className="text-left">
                  <img className='w-5 opacity-70' src={CreateIcon} />
                </span>
                <span className="mx-4 text-sm font-normal">
                  Add Job
                </span>
              </Link>
              <Link to="/dashboard/profil" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                <span className="text-left">
                  <img className='w-5' src={Profile} />
                </span>
                <span className="mx-4 text-sm font-normal">
                  Profile
                </span>
              </Link>
              <Link to="/dashboard/password" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                <span className="text-left">
                  <img className='w-5 opacity-70' src={Lock} />
                </span>
                <span className="mx-4 text-sm font-normal">
                  Change Password
                </span>
              </Link>

            </div>
          </nav>
        </div>
      </div>
      <div className="sm:hidden">
        <div className={showSidebar ? "h-screen sm:hidden shadow-lg relative w-96" : "w-10"}>
          <div className="-mr-2  flex ">
            <button onClick={() => {
              showSidebar ? setShowSidebar(false) : setShowSidebar(true)
            }} className="text-gray-800 dark:text-white hover:text-gray-300 inline-flex items-center justify-center p-2 rounded-md focus:outline-none">
              <svg width={20} height={20} fill="currentColor" className="h-8 w-8" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1664 1344v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45z">
                </path>
              </svg>
            </button>
          </div>
          <div className={showSidebar ? "" : "hidden"}>
            <div className="bg-white h-full rounded-2xl dark:bg-gray-700">
              <div className="flex items-center justify-center pt-6">

              </div>
              <nav className="mt-6">
                <div>
                  <Link to="/dashboard" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                    <span className="text-left">
                      <svg width={20} height={20} fill="currentColor" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1070 1178l306-564h-654l-306 564h654zm722-282q0 182-71 348t-191 286-286 191-348 71-348-71-286-191-191-286-71-348 71-348 191-286 286-191 348-71 348 71 286 191 191 286 71 348z">
                        </path>
                      </svg>
                    </span>
                    <span className="mx-4 text-sm font-normal">
                      Dashboard
                    </span>
                  </Link>
                  <Link to="/dashboard/list-job-vacancy" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                    <span className="text-left">
                      <svg width={20} height={20} fill="currentColor" className="m-auto" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1024 1131q0-64-9-117.5t-29.5-103-60.5-78-97-28.5q-6 4-30 18t-37.5 21.5-35.5 17.5-43 14.5-42 4.5-42-4.5-43-14.5-35.5-17.5-37.5-21.5-30-18q-57 0-97 28.5t-60.5 78-29.5 103-9 117.5 37 106.5 91 42.5h512q54 0 91-42.5t37-106.5zm-157-520q0-94-66.5-160.5t-160.5-66.5-160.5 66.5-66.5 160.5 66.5 160.5 160.5 66.5 160.5-66.5 66.5-160.5zm925 509v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm0-260v-56q0-15-10.5-25.5t-25.5-10.5h-568q-15 0-25.5 10.5t-10.5 25.5v56q0 15 10.5 25.5t25.5 10.5h568q15 0 25.5-10.5t10.5-25.5zm0-252v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm256-320v1216q0 66-47 113t-113 47h-352v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-768v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-352q-66 0-113-47t-47-113v-1216q0-66 47-113t113-47h1728q66 0 113 47t47 113z">
                        </path>
                      </svg>
                    </span>
                    <span className="mx-4 text-sm font-normal">
                      Job List
                    </span>
                  </Link>
                  <Link to="/dashboard/create" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                    <span className="text-left">
                      <img className='w-5 opacity-70' src={CreateIcon} />
                    </span>
                    <span className="mx-4 text-sm font-normal">
                      Add Job
                    </span>
                  </Link>
                  <Link to="/dashboard/profil" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                    <span className="text-left">
                      <img className='w-5' src={Profile} />
                    </span>
                    <span className="mx-4 text-sm font-normal">
                      Profile
                    </span>
                  </Link>
                  <Link to="/dashboard/password" className="w-full font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center p-4 my-2 transition-colors duration-200 justify-start hover:text-blue-500 hover:bg-gradient-to-r from-white to-blue-100">
                    <span className="text-left">
                      <img className='w-5 opacity-70' src={Lock} />
                    </span>
                    <span className="mx-4 text-sm font-normal">
                      Change Password
                    </span>
                  </Link>

                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}

export default Sidebar;